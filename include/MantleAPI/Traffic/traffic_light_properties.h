/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  traffic_light_properties.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_TRAFFIC_LIGHT_PROPERTIES_H
#define MANTLEAPI_TRAFFIC_TRAFFIC_LIGHT_PROPERTIES_H

#include <MantleAPI/Common/time_utils.h>
#include <vector>

namespace mantle_api
{
enum class TrafficLightBulbColor
{
    kUnknown = 0,
    kOther = 1,
    kRed = 2,
    kYellow = 3,
    kGreen = 4,
    kBlue = 5,
    kWhite = 6
};

enum class TrafficLightBulbMode
{
    kUnknown = 0,
    kOther = 1,
    kOff = 2,
    kConstant = 3,
    kFlashing = 4,
    kCounting = 5
};

struct TrafficLightBulbState
{
    TrafficLightBulbColor color;
    TrafficLightBulbMode mode;
};

struct TrafficLightPhase
{
    std::vector<TrafficLightBulbState> bulb_states;
    mantle_api::Time start_time{0};
    mantle_api::Time end_time{0};
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_TRAFFIC_LIGHT_PROPERTIES_H
